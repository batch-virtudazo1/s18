console.log("Hello World");

function sumResult(num1, num2){
 let sum = num1 + num2;
 console.log("Displayed sum of " + num1 + " and " + num2 + " is:" + '\n' + sum);
}
sumResult(5,15);

function differenceResult(num1, num2){
 let difference = num1 - num2;
 console.log("Displayed difference of " + num1 + " and " + num2 + " is:" + '\n' + difference);
}
differenceResult(20,5);


function productResult (num1, num2){
	let product = num1 * num2;

	return "The product of " + num1 + " and " + num2 + ':\n' + product ;
}

let product = productResult(50,10);
console.log(product);

function quotientResult (num1, num2){
	let quotient = num1 / num2;

	return "The quotient of " + num1 + " and " + num2 + ':\n' + quotient ;
}

let quotient = quotientResult(50,10);
console.log(quotient);

function circleAreaResult(radiusInput){
	let area = 3.14 * (radiusInput**2);
	return "The result in getting the area of a circle with " + radiusInput + " radius:\n"
	+ area;
}

let circleArea = circleAreaResult(15);
console.log(circleArea);

function averageResult(num1, num2, num3, num4) {
	let average = (num1 + num2 + num3 + num4)/4;
	return "The average of " + num1 + ", " + num2 + ", " + num3 + " and "
	 + num4 + ":\n" + average
}

let averageVar = averageResult(20,40,60,80);
console.log(averageVar);

function checker(score, total){
	let percentage = (score / total)*100;
	let isPassed = percentage >= 75;
	return "Is " + score + "/" + total + " a passing score? \n" + isPassed;
}

let isPassingScore = checker(38,50);
console.log(isPassingScore);